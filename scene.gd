extends Node2D

func _on_Area2D_input_event( viewport, event, shape_idx ):
	if event is InputEventMouseButton and event.is_pressed():
		scale = Vector2(2, 2) if scale == Vector2(1, 1) else Vector2(1, 1)